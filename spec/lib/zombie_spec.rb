###
# Sample spec for first Rspec use case
# (c) Saul Costa
# 25 Jan 2014
###

require "spec_helper"
require "zombie"

describe Zombie do
  # examples go here
  xit "is named Ash" do
    zombie = Zombie.new
    zombie.name.should == 'Ash'
  end

  it "has no brains" do
    zombie = Zombie.new
    zombie.brains.should < 1
  end

  it "should be dead" do
    zombie = Zombie.new
    zombie.alive.should be_false
  end
end