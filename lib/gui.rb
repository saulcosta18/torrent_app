###
# This provides functionality for interfacing with the GUI
# It will allow the user to:
# => Change settings (see db/schema.rb)
# => Control movies that will be downloaded
###

# TODO: This is how to corectly require the directories.
# => Put it in the main script (this script should also start Shoes)
$: << File.expand_path(File.dirname(__FILE__))
$: << File.expand_path(File.dirname(__FILE__) + "/db")

require "schema"
require "api"
require "green_shoes"

module Gui

  def initialize
    @api = API.new
  end

  # Ask the user if they want to queue the listed movies (after getting them from RT/KAT)
  def prompt_user(movie_titles)
    movies = movie_titles
    Shoes.app width: 500, height: 500, title: "Pick movies to download!" do
      background rgb(171, 236, 245)
      stack top: 0, left: 0, margin: 25 do
        para "Choose the movies you want to download"
        movies.map! do |movie|
          flow { @c = check; para "%s - (%d)" % [movie[:title], movie[:year]]; @c.checked = true } 
          [@c, movie]
       end
      end
      button "Accept Selection" do
        selected = moives.map { |c, movie| movie if c.checked? }.compact
        selected.each do |movie|
          Queued.create(title: movie[:title], year: movie[:year])
        end
        alert "Movies added to download queue"
      end
    end
  end

  # All settings for the app. Shown on main page.
  def settings
    u_settings = Settings.first
    stack margin_right: 10, margin_left: 50, width: 300, top: 50, left: 0 do
      inscription "Kickass.to Download Path"
      kat_path = edit_line
      kat_path.text = u_settings.kat_path

      inscription "Total allowed downloads"
      max_total_downloads = edit_line
      max_total_downloads.text = u_settings.max_total_downloads.to_s

      inscription "Minimum seeds for a torrent"
      min_seeds = edit_line
      min_seeds.text = u_settings.min_seeds.to_s

      new_releases = check; inscription "Download new releases?"
      new_releases.checked = u_settings.new_releases

      already_released = check; inscription "Download past releases?"
      already_released.checked = u_settings.already_released

      old_movies = check; inscription "Download old movies?"
      old_movies.checked = u_settings.old_movies

      auto_download = check; inscription "Download movies automatically?"
      auto_download.checked = u_settings.auto_download

      button "Save" do
        Settings.update(u_settings.id,
          kat_path: kat_path.text,
          max_total_downloads: max_total_downloads.text.to_i,
          min_seeds: min_seeds.text.to_i,
          new_releases: new_releases.checked?,
          already_released: already_released.checked?,
          old_movies: old_movies.checked?,
          auto_download: auto_download.checked?
        )
        alert "Settings saved"
      end
    end
  end

  def mpaa
    u_settings = Settings.first
    mpaa_ratings = ["g", "pg", "pg_13", "r", "x"]
    Shoes.app width: 400, height: 450, title: "Downloaded Movies" do
      background rgb(171, 236, 245)
      stack top: 25, margin_left: 25 do
        para "MPAA Settings"
        mpaa_ratings.map! do |rating|
          flow { @c = check; inscription rating.capitalize; @c.checked = u_settings[rating] }
          [@c, rating]
        end
        button "Save" do
          selected = mpaa_ratings.map { |c, rating| rating if c.checked? }.compact
          Settings.update(u_settings.id,
            g: selected.include?("g"),
            pg: selected.include?("pg"),
            pg_13: selected.include?("pg_13"),
            r: selected.include?("r"),
            x: selected.include?("x")
          )
          alert "Settings saved"
        end
      end
    end
  end

  # List all the queued movies and their status and allow user to remove titles
  # TODO: add a status to each item in the queue (not/downloading)
  def queued
    Shoes.app width: 400, height: 450, title: "Queued Movies" do
      background rgb(171, 236, 245)
      q_movies = Queued.all.order(:title)
      if q_movies.length > 0
        stack top: 15, left: 0, margin: 25 do
          para "All movies pending downloading"
          inscription "Remove by checking the boxes, then hit 'remove'"
          q_movies.map! do |movie|
            flow { @c = check; inscription "%s - (%d)" % [movie[:title], movie[:year]], width: 350 }
            [@c, movie[:id]]
          end
          button "Remove" do
            if confirm("Are you sure?")
              selected = q_movies.map { |c, id| id if c.checked? }.compact
              selected.each do |id|
                Queued.delete(id)
              end
            end
          end
        end
      else
        stack margin_left: 10, margin_top: 20 do
          para "No movies pending download"
          inscription "Enable automatic downloads or manually download titles"
        end
      end
    end
  end

  # List all downloaded movies
  # (?) Allow them to remove movies from here
  def downloaded
    Shoes.app width: 400, height: 450, title: "Downloaded Movies" do
      background rgb(171, 236, 245)
      d_movies = Downloaded.all.order(:title)
      if d_movies.length > 1
        stack top: 15, left: 0, margin: 25 do
          para "All downloaded movies"
          d_movies.each do |movie|
            inscription "%s - %s" % [movie[:title], movie[:year]]
          end
        end
      else
        stack margin_left: 10, margin_top: 20 do
          para "No downloaded movies"
          inscription "Enable automatic downloads or manually download titles to build up your library!"
        end
      end
    end
  end

  # Specifications pertaining to disk space used, number of movies, etc.
  def specs
    Shoes.app width: 400, height: 450, title: "Specs" do
      background rgb(171, 236, 245)
      stack margin_left: 10, margin_top: 20 do
        para "Coming soon!"
      end
    end
  end

  # Manually update KAT list and queue moives
  def manual
    # TODO: account for other movies (previously released/old)
    movie_data = @api.in_theatres 
    Shoes.app width: 400, height: 450, title: "Specs" do
      stack margin_left: 10, margin_top: 20 do
        para "Select desired movies"
        movie_data.map! do |movie|
          # TODO: Finish
        end
      end
    end
  end

  # About information
  def about
    Shoes.app width: 400, height: 200, title: "About" do
      background rgb(171, 236, 245)
      stack margin_left: 10, top: 10 do
        para "By using this application you agree to the following:", align: "center"
        inscription "This application is not meant to promote any form of illegal activity. It is meant as a case study only.", align: "center"
        inscription "\n\n\nCreated by Saul Costa", align: "center"
        inscription "Released under MIT license", align: "center"
      end
    end
  end

end

Shoes.app width: 500, height: 400, title: "TorrentApp Settings" do
  extend Gui
  background rgb(171, 236, 245)

  stack margin_left: 5, margin_right: 25, width: 100 do
    @queued = button "Queued"
    @queued.move(5, 10)
    @queued.click{queued}

    @downloaded = button "Downloaded"
    @downloaded.move(5, 50)
    @downloaded.click{downloaded}

    @specs = button "Specs"
    @specs.move(5, 90)
    @specs.click{specs}

    @mpaa = button "MPAA"
    @mpaa.move(5, 130)
    @mpaa.click{mpaa}

    @manual = button "Manual"
    @manual.move(5, 170)
    @manual.click{manual}

    @about = button "About"
    @about.move(5, 350)
    @about.click{about}
  end

  settings

end