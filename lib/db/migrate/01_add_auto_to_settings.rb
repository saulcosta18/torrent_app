require "active_record"

ActiveRecord::Base.establish_connection(
    adapter: "sqlite3",
    database: "sqlite.db"
)

class AddAutoToSettings < ActiveRecord::Migration
  def change
    add_column :settings, :auto_download, :boolean
  end
end
migration = AddAutoToSettings.new
migration.change