require_relative './config'

class Queued < ActiveRecord::Base
  self.table_name = "queued"
end

class Downloaded < ActiveRecord::Base
  self.table_name = "downloaded"
end

class Settings < ActiveRecord::Base
  self.table_name = "settings"
end

if Settings.first.nil?
  Settings.create(
    kat_path: "./tmp/hourlydump.txt",
    max_total_downloads: 100,
    min_seeds: 20,
    g: true,
    pg: true,
    pg_13: true,
    r: true,
    x: true,
    new_releases: true,
    already_released: true,
    old_movies: false
  )
end