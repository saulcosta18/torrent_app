require "active_record"

ActiveRecord::Base.logger = Logger.new(STDERR)

ActiveRecord::Base.establish_connection(
    adapter: "sqlite3",
    database: "sqlite.db"
)
 
ActiveRecord::Schema.define do
  unless ActiveRecord::Base.connection.tables.include? "queued"
    create_table :queued do |table|
        table.column :title, :string
        table.column :year, :integer
    end
  end

  unless ActiveRecord::Base.connection.tables.include? "downloaded"
    create_table :downloaded do |table|
        table.column :title, :integer
        table.column :year, :integer
    end
  end

  unless ActiveRecord::Base.connection.tables.include? "settings"
    create_table :settings do |table|
        table.column :kat_path, :string
        table.column :max_total_downloads, :integer
        table.column :min_seeds, :integer
        table.column :g, :boolean
        table.column :pg, :boolean
        table.column :pg_13, :boolean
        table.column :r, :boolean
        table.column :x, :boolean
        table.column :new_releases, :boolean
        table.column :already_released, :boolean
        table.column :old_movies, :boolean
        table.column :auto_download, :boolean
    end
  end
end

