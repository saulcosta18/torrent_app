# Format of data:
# id|title|category|url|torrent_url

# Steps this will perform:
# => Check if there is an updated list of torrents
# => If there is, delete the old list and process this list for new movies to download
# => Add each of these movies to the database ("to_download" table) if they aren"t in there or in the "downloaded" table

$: << File.expand_path(File.dirname(__FILE__))
$: << File.expand_path(File.dirname(__FILE__) + "/db")

require "date"
require "http"
require "open-uri"
require "zlib"
require "schema"

class ProcessData

  # Note that for now file_path will be set to whatever we want it to be
  def initialize
    @file_path = "file_path"
  end

  def update_file
    # If @file_path is outdated remove it and download the new one.
    # Else do nothing
    last_modified = DateTime.parse(HTTP.get("http://kickass.to/hourlydump.txt.gz").response.headers["Last-Modified"])
    local_last_modified = File.mtime(@file_path)
    # Check to see if the file we have is outdated
    # if local_last_modified < last_modified - 30.minutes
    if false
      url = "http://kickass.to/hourlydump.txt.gz"
      source = open(url)
      gz = Zlib::GzipReader.new(source) 
      result = gz.read
      File.open("/Users/scosta/Programming/torrent_app/hourlydump.txt", "w") {|f| f.write(result) }
    end
    fd = File.open("/Users/scosta/Programming/torrent_app/hourlydump.txt", "r")
    fd.each do |line|
      data = line.split("|")
      if data[2] == "Movies"
        puts data[1]
      end
    end
  end

  # Filter out any movies with an (audiance_score + critics_score)/2 < score
  # Input: [movie_data], score
  # Output: [filtered_movie_names]
  def satisfy_ratings(movie_data, score)
    movie_data.select {|m| (m["ratings"]["critics_score"].to_i + m["ratings"]["audience_score"].to_i) / 2 > score}
  end

  # Check if the user wants to add the movie if it satisfies their settings
  # and is not already in the database
  # Input: [movie_data]
  # Output: success or error message for GUI
  def updated_data(movie_data)
    movie_titles = []
    settings = Settings.first
    movie_data.each do |movie|
      if Queued.find_by_title_and_year(movie["title", movie["year"]]).nil? and Downloaded.find_by_title_and_year(movie["title"], movie["year"]).nil?
        mpaa_rating = movie["mpaa_rating"]
        case mpaa_rating
        when "G"
          settings.g ? movie_titles.push(title: movie["title"], year: movie["year"]) : next
        when "PG"
          settings.pg ? movie_titles.push(title: movie["title"], year: movie["year"]) : next
        when "PG-13"
          settings.pg_13 ? movie_titles.push(title: movie["title"], year: movie["year"]) : next
        when "R"
          settings.r ? movie_titles.push(title: movie["title"], year: movie["year"]) : next
        when "X"
          settings.x ? movie_titles.push(title: movie["title"], year: movie["year"]) : next
        else
          movie_titles.push(title: movie["title"], year: movie["year"])
        end
      end
    end
    movie_titles
  end

  def add_to_database(title, year)
    Queued.create(title: title, year: year)
  end

end