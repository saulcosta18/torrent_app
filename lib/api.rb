###
# This class provides functionality for interfacing with the
# RottenTomatoes JSON API
###

# TODO: see if I actually have to re-require these here.

$: << File.expand_path(File.dirname(__FILE__))
$: << File.expand_path(File.dirname(__FILE__) + "/db")

require "json"
require "net/http"
require "uri"
require "process_data"

class API

  def initialize
    @base_url = "http://api.rottentomatoes.com/api/public/v1.0/lists/movies/%s.json?apikey=%s&page_limit=1"
    @api_key = "xfu3r75yqd5tdqu4jc6g57h4"
    @pd = ProcessData.new
  end

  # Function for getting movies in theatres
  # Input: minimum_rating
  # Output: array of movie titles
  def in_theatres
    url = URI.parse(@base_url % ["in_theatres", @api_key])
    http = Net::HTTP.new(url.host, url.port)
    request = Net::HTTP::Get.new(url.request_uri)
    response = http.request(request)

    if response.code == "200"
      result = JSON.parse(response.body)
      filtered = @pd.satisfy_ratings(result["movies"])
      if filtered.length > 0
        movie_data = @pd.updated_data(filtered)
        # TODO: trigger GUI alert with status (string)
      end
    else
      raise("Could not get RT data.")
    end
    movie_data
  end

end